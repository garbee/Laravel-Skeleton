# Laravel Skeleton

This is a personal Laravel project skeleton.
Included are:

* View composers
* Model Observers
* Twig for templates
* Routes defined via provider classes
* PHPCS and PHPMD Linting
* ghooks via npm for git hooks tracked in repository
* Stylelint config for verifying CSS structure.
* Eslint config for JavaScript linting.

