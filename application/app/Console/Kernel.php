<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\Make\Observer::class,
        Commands\Make\Service::class,
        Commands\Make\Input::class,
        Commands\Make\Composer::class,
        Commands\Make\Exception::class,
        Commands\Make\Transformer::class,
        Commands\Make\RouteProvider::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     *
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
    }
}
