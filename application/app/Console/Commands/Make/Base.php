<?php

namespace App\Console\Commands\Make;

use Illuminate\Console\GeneratorCommand;

abstract class Base extends GeneratorCommand
{
    protected function getStubPath(string $filename) : string
    {
        return __DIR__ . implode(DIRECTORY_SEPARATOR, ['', 'stubs', $filename]);
    }
}
