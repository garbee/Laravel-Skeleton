<?php

namespace App\Console\Commands\Make;

class Transformer extends Base
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:transformer {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new transformer class';

    protected function replaceClass($stub, $name)
    {
        $stub = parent::replaceClass($stub, $name);
        $stub = str_replace('DummyReference', $this->argument('name'), $stub);

        return $stub;
    }

    protected function getStub() : string
    {
        return $this->getStubPath('transformer.php.stub');
    }

    protected function getDefaultNamespace($rootNamespace) : string
    {
        return $rootNamespace . '\Transformers';
    }
}
