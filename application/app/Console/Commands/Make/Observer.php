<?php

namespace App\Console\Commands\Make;

class Observer extends Base
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:observer {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new model observer class';

    protected function getStub() : string
    {
        return $this->getStubPath('observer.php.stub');
    }

    protected function getDefaultNamespace($rootNamespace) : string
    {
        return $rootNamespace . '\Observers';
    }

    protected function replaceClass($stub, $name)
    {
        $stub = parent::replaceClass($stub, $name);
        $stub = str_replace('DummyReference', $this->argument('name'), $stub);

        return $stub;
    }
}
