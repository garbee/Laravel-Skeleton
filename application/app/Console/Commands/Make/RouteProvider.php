<?php

namespace App\Console\Commands\Make;

class RouteProvider extends Base
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:routeProvider {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new route provider class';

    protected function getStub() : string
    {
        return $this->getStubPath('routeProvider.php.stub');
    }

    protected function getDefaultNamespace($rootNamespace) : string
    {
        return $rootNamespace . '\Providers\Routes';
    }
}
