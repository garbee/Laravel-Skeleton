<?php

namespace App\Console\Commands\Make;

class Service extends Base
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:service {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new service class';

    protected function getStub()
    {
        return $this->getStubPath('service.php.stub');
    }

    protected function getDefaultNamespace($rootNamespace) : string
    {
        return $rootNamespace . '\Services';
    }
}
