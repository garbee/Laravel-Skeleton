<?php

namespace App\Console\Commands\Make;

class Composer extends Base
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:composer {name} {--view=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new view composer class';

    protected function getDefaultNamespace($rootNamespace) : string
    {
        return $rootNamespace . '\Composers';
    }

    protected function getStub() : string
    {
        return $this->getStubPath('composer.php.stub');
    }

    protected function replaceClass($stub, $name)
    {
        $stub = parent::replaceClass($stub, $name);

        $appliesTo = '';
        if ($this->hasOption('view')) {
            $appliesTo = $this->option('view');
        }

        $stub = str_replace('DummyApplies', $appliesTo, $stub);

        return $stub;
    }
}
