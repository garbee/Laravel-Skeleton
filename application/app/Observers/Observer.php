<?php

namespace App\Observers;

interface Observer
{
    /**
     * Get the model class that the observer handles.
     *
     * @return string
     */
    public static function appliesTo() : string;
}
