<?php

namespace App\Composers\Pages;

use App\Composers\Composer;
use Illuminate\Contracts\View\View;

class Home implements Composer
{
    public function compose(View $view)
    {
        $view->with('name', 'Some name');
    }

    public static function appliesTo() : string
    {
        return 'welcome';
    }
}
