<?php

namespace App;

use ReflectionClass;
use Composer\Autoload\ClassLoader;
use Illuminate\Support\Collection;

class Composer
{
    /**
     *  The valid types that can be filtered out of results.
     */
    const VALIDTYPES = [
        'Interface',
        'Abstract',
        'Trait',
    ];

    /** @var ClassLoader $loader */
    private $loader;

    /** @var Collection $classMap */
    private $classMap;

    public function __construct()
    {
        $this->loader = require(base_path('vendor/autoload.php'));
        $this->classMap = collect($this->loader->getClassMap());
    }

    /**
     * Get all registered classes.
     *
     * @return Collection
     */
    public function classMap() : Collection
    {
        return $this->classMap;
    }

    /**
     * Get classes registered that are within the provided namespace.
     *
     * @param string $namespace The namespace to filter items for.
     *
     * @return Collection
     */
    public function inNamespace(string $namespace) : Collection
    {
        return $this->classMap->filter(function(string $path, string $class) use ($namespace) {
            return starts_with($class, $namespace);
        });
    }

    /**
     * Get all items in the given namespace that can be instantiated.
     *
     * This removes all interfaces, abstract classes, traits, and classes with private constructors.
     *
     * @param string $namespace The namespace to filter items for.
     *
     * @return Collection
     */
    public function inNamespaceAndInstantiable(string $namespace) : Collection
    {
        return $this->inNamespace($namespace)->filter(function(string $path, string $class) {
            $reflection = new ReflectionClass($class);
            return $reflection->isInstantiable();
        });
    }

    /**
     *
     * @param string $namespace The namespace to filter items for.
     * @param string|array $type The type of items to filter out. Must be one of self::VALIDTYPES.
     *
     * @return Collection
     */
    public function inNamespaceWithoutType(string $namespace, $type) : Collection
    {
        $type = collect($type)->map(function(string $item) {
            return trim(ucfirst($item));
        });

        $type->each(function(string $item) {
            if (in_array($item, self::VALIDTYPES)) {
                throw new \InvalidArgumentException();
            }
        });

        return $this->inNamespace($namespace)->filter(function(string $path, string $class) use ($type) {
            $reflection = new ReflectionClass($class);
            $classIsNotOfType = true;

            $type->each(function(string $item) use (&$classIsNotOfType, $reflection) {
                if ($reflection->{'is' . $item}) {
                    $classIsNotOfType = false;
                }
            });

            return $classIsNotOfType;
        });
    }
}
