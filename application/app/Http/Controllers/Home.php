<?php

namespace App\Http\Controllers;

use Symfony\Component\HttpFoundation\Response;

class Home extends Controller
{
    public function index() : Response
    {
        return $this->response->view('welcome');
    }
}
