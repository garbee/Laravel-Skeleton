<?php

namespace App\Providers;

use App\Composer as Loader;
use App\Observers\Observer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\ServiceProvider;

class Observers extends ServiceProvider
{
    public function boot()
    {
        (new Loader())
            ->inNamespaceAndInstantiable('App\\Observers')
            ->keys()
            ->each(function(string $class) {
                /** @var Observer $class */
                /** @var Model $model */
                $model = $class::appliesTo();
                $reflection = new \ReflectionClass($class);
                if (class_exists($model)
                    && $reflection->implementsInterface(Observer::class)
                ) {
                    $model::observe($class);
                }
            });
    }
}
