<?php

namespace App\Providers\Routes;

abstract class Base implements RouteProvider
{
    /**
     * Get the format for a string callable for a route.
     *
     * @param string $controller
     * @param string $method
     * @return string
     */
    protected static function uses(string $controller, string $method) : string
    {
        return $controller . '@' . $method;
    }
}
