<?php

namespace App\Providers\Routes;

use Illuminate\Routing\Router;

interface RouteProvider
{
    /**
     * Register any API routes needed.
     *
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     *
     * @param Router $router
     * @return void
     */
    public static function registerApiRoutes(Router $router);

    /**
     * Register any Web routes needed.
     *
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     *
     * @param Router $router
     * @return void
     */
    public static function registerWebRoutes(Router $router);
}
