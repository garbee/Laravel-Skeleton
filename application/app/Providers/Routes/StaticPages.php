<?php

namespace App\Providers\Routes;

use App\Http\Controllers\Home;
use Illuminate\Routing\Router;

class StaticPages extends Base
{

    /**
     * @inheritdoc
     */
    public static function registerApiRoutes(Router $router)
    {
    }

    /**
     * @inheritdoc
     */
    public static function registerWebRoutes(Router $router)
    {
        $router->get('/', [
            'as' => 'home',
            'uses' => self::uses(Home::class, 'index'),
        ]);
    }
}
