<?php

namespace App\Providers;

use App\User;
use Illuminate\Broadcasting\BroadcastManager;
use Illuminate\Support\ServiceProvider;

class Broadcast extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /** @var BroadcastManager $broadcaster */
        $broadcaster = $this->app->make(BroadcastManager::class);
        $broadcaster->routes();

        /*
         * Authenticate the user's personal channel...
         */
        $broadcaster->connection()->channel('App.User.*', function (User $user, int $userId) {
            return $user->id === $userId;
        });
    }
}
