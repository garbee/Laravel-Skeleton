<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Support\Collection;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class Route extends ServiceProvider
{
    /** @var Router $router */
    private $router;

    /** @var Collection $routeProviders */
    private $routeProviders;

    /** @var array $providers */
    private $providers = [
        Routes\StaticPages::class,
    ];

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->router = $this->app['router'];
        $this->routeProviders = new Collection($this->providers);

        $this->mapWebRoutes();

        $this->mapApiRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        $this->router->group([
            'middleware' => 'web',
        ], function () {
            $this->routeProviders->each(function (string $providerClass) {
                /** @var Routes\RouteProvider $providerClass */
                $providerClass::registerWebRoutes($this->router);
            });
        });
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        $this->router->group([
            'prefix'     => 'api',
        ], function () {
            $this->routeProviders->each(function (string $providerClass) {
                /** @var Routes\RouteProvider $providerClass */
                $providerClass::registerApiRoutes($this->router);
            });
        });
    }
}
