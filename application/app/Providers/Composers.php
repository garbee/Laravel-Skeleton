<?php
namespace App\Providers;

use App\Composer as Loader;
use App\Composers\Composer;
use Illuminate\View\Factory;
use Illuminate\Support\ServiceProvider;

class Composers extends ServiceProvider
{
    public function register()
    {
        $this->registerComposers($this->app['view']);
    }
    private function registerComposers(Factory $view)
    {
        (new Loader())
            ->inNamespaceAndInstantiable('App\\Composers')
            ->keys()
            ->each(function(string $class) use ($view) {
                /** @var Composer $class */
                $reflection = new \ReflectionClass($class);
                if ($reflection->implementsInterface(Composer::class)) {
                    $view->composer($class::appliesTo(), $class);
                }
            });
    }
}
