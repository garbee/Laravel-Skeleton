<?php

namespace App\Tests\Features;

use App\Tests\BaseCase;

class HomepageTest extends BaseCase
{
    public function testHomePageRenders()
    {
        $this->visit('/')
             ->see('Laravel');
    }
}
